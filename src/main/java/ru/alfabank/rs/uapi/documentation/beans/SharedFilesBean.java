package ru.alfabank.rs.uapi.documentation.beans;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletContext;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author akovalev aekovalev@alfabank.ru
 */
@Named
@RequestScoped
public class SharedFilesBean {

    private final StreamedContent phpExample;
    private final StreamedContent uapiCollection;
    private final StreamedContent uapiEnviroment;

    public SharedFilesBean() {
        ServletContext context = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
        phpExample = new DefaultStreamedContent(context.getResourceAsStream("/resources/sharedFiles/p2p_uapi.php"),
                "text/plain", "p2p_uapi.php");
        uapiEnviroment = new DefaultStreamedContent(context.getResourceAsStream("/resources/sharedFiles/testjmb_TEST.postman_environment"),
                "text/plain", "testjmb_TEST.postman_environment");
        uapiCollection = new DefaultStreamedContent(context.getResourceAsStream("/resources/sharedFiles/uapi.json.postman_collection"),
                "text/plain", "uapi.json.postman_collection");
    }

    public StreamedContent getPhpExample() {
        return phpExample;
    }

    public StreamedContent getUapiCollection() {
        return uapiCollection;
    }

    public StreamedContent getUapiEnviroment() {
        return uapiEnviroment;
    }
}